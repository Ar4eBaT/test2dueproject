//
//  CubeSpritesUpload.h
//  TestProject
//
//  Created by Arthur Saprykin on 26.08.15.
//  Copyright (c) 2015 EpicGames. All rights reserved.
//

#ifndef TestProject_CubeSpritesUpload_h
#define TestProject_CubeSpritesUpload_h

#include "ConstructorHelpers.h"

typedef ConstructorHelpers::FObjectFinderOptional<UPaperSprite> SpriteFindObject;

#define FirstCubeSprite TEXT("/Game/Sprites/FirstCubeSprite.FirstCubeSprite")
#define SecondCubeSprite TEXT("/Game/Sprites/secondBox_Sprite_0.secondBox_Sprite_0")
#define ThirdCubeSprite TEXT("/Game/Sprites/thirdBox_Sprite_0.thirdBox_Sprite_0")
#define FourthCubeSprite TEXT("/Game/Sprites/fourthBox_Sprite_0.fourthBox_Sprite_0")
#define FifthCubeSprite TEXT("/Game/Sprites/fifthBox_Sprite_0.fifthBox_Sprite_0")
#define SixthCubeSprite TEXT("/Game/Sprites/sixthBox_Sprite_0.sixthBox_Sprite_0")

static SpriteFindObject CubeSpritesUpload(int Item) {

    switch (Item) {
        case 0:
            return SpriteFindObject(FirstCubeSprite);
            break;
        case 1:
            return SpriteFindObject(SecondCubeSprite);
            break;
        case 2:
            return SpriteFindObject(ThirdCubeSprite);
            break;
        case 4:
            return SpriteFindObject(FourthCubeSprite);
            break;
        case 5:
            return SpriteFindObject(FifthCubeSprite);
            break;
        default:
            return SpriteFindObject(SixthCubeSprite);
            break;
    }
}

#endif
