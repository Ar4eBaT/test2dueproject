// Fill out your copyright notice in the Description page of Project Settings.

#include "TestProject.h"
#include "TileSpriteControllerActor.h"
#include "CubeSpritesUpload.h"

ATileSpriteControllerActor::ATileSpriteControllerActor() {
    
    PrimaryActorTick.bCanEverTick = true;
    
//    for (int i = 0; i < 10; i++) {
//        int32 random = rand() % 6;
//        FName ComponentName = FName(TEXT("SpriteComponent"), i);
//        SpriteComponent.Insert(CreateDefaultSubobject<UPaperSpriteComponent>(ComponentName), i);
//        SpriteComponent[i]->SetSprite(CubeSpritesUpload(random).Get());
//        SpriteComponent[i]->AttachTo(RootComponent);
//        FVector2D location = FVector2D(GetActorLocation().X + i * 150.0f, GetActorLocation().Y);
//        SpriteComponent[i]->RelativeLocation = FVector(location, GetActorLocation().Z);
//        SpriteComponent[i]->SetConstraintMode(EDOFMode::XZPlane);
//        
////        SpriteComponent[i]->SetSimulatePhysics(true);
//    }
//    
//    if (SpriteComponent[5] != NULL) {
//        SpriteComponent[5]->DestroyComponent();
//    }
//    
//    SpriteComponent.RemoveAt(5);
}

void ATileSpriteControllerActor::BeginPlay() {
    Super::BeginPlay();
    
//    FVector Vect = FVector(GetActorLocation().X + 1 * 150.0f, GetActorLocation().Y - 150.0f * 3, GetActorLocation().Z );
//    FRotator Rotator = FRotator(0.0, 0.0, 0.0);
//    SpriteComponent.Insert(GetWorld()->SpawnActor<AMyPaperSpriteActor>(Vect, Rotator), 0);
}

void ATileSpriteControllerActor::MouseClick(int32 count) {
    UE_LOG(LogTemp, Error, TEXT("Count : %d"), TileActors[count]->GetNumber());
    UE_LOG(LogTemp, Error, TEXT("Owner : %d"), Children.Num());

}
