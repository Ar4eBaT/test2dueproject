// Fill out your copyright notice in the Description page of Project Settings.

#include "TestProject.h"
#include "RandomNumberGameMode.h"

void ARandomNumberGameMode::BeginPlay() {
    Super::BeginPlay();
    ChangeMenuWidget(StartingWidgetClass);
}

void ARandomNumberGameMode::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass) {
    if (NewWidgetClass != nullptr) {
        UUserWidget* CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
        if (CurrentWidget != nullptr) {
            CurrentWidget->AddToViewport();
        }
    }
}


void ARandomNumberGameMode::ChangeRandomNumber() {
    UWorld* GameWorld = GetWorld();
    for (TObjectIterator<AMyPaperSpriteActor> Iterator; Iterator; ++Iterator) {
        if (Iterator->GetWorld() != GameWorld) {
            continue;
        }
        
        Iterator->SetText();
    }
}


