// Fill out your copyright notice in the Description page of Project Settings.

#include "TestProject.h"
#include "MyPaperSpriteActor.h"
#include "Kismet/GameplayStatics.h"


AMyPaperSpriteActor::AMyPaperSpriteActor() {
    
    PrimaryActorTick.bCanEverTick = true;
    
    typedef ConstructorHelpers::FObjectFinderOptional<UPaperSprite> SpriteFindObject;
    
    SpriteFindObject Sprite = SpriteFindObject(TEXT("/Game/Sprites/VoidCubeSprite.VoidCubeSprite"));
    
    GetRenderComponent()->SetSprite(Sprite.Get());
    RootComponent = GetRenderComponent();

    NumberText = CreateDefaultSubobject<UTextRenderComponent>(TEXT("NumberText"));
    NumberText->SetWorldSize(100.0f);
    NumberText->SetTextRenderColor(FColor(50));
    SetText();
    
    NumberText->AttachTo(RootComponent);
    NumberText->RelativeRotation = FRotator(0.0f, 90.0f, 0.0f);
    NumberText->SetHorizontalAlignment(EHTA_Center);
    FVector2D location = FVector2D(GetActorLocation().X, GetActorLocation().Y + 10.0f);
    NumberText->RelativeLocation = FVector(location, GetActorLocation().Z - 50);
}

void AMyPaperSpriteActor::BeginPlay() {
    Super::BeginPlay();
}

void AMyPaperSpriteActor::Tick(float DeltaSeconds) {
    Super::Tick(DeltaSeconds);
}

void AMyPaperSpriteActor::SetText() {
    Number = rand() % 6 + 1;
    NumberText->SetText(FString::FromInt(Number));
}

int32 AMyPaperSpriteActor::GetNumber() const {
    return Number;
}