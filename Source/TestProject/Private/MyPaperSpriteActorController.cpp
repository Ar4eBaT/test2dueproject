// Fill out your copyright notice in the Description page of Project Settings.

#include "TestProject.h"
#include "MyPaperSpriteActorController.h"



AMyPaperSpriteActorController::AMyPaperSpriteActorController() {
    
    PrimaryActorTick.bCanEverTick = true;
    
    typedef ConstructorHelpers::FObjectFinderOptional<UPaperSprite> SpriteFindObject;
    
    SpriteFindObject Sprite = SpriteFindObject(TEXT("/Game/Sprites/VoidCubeSprite.VoidCubeSprite"));
    SpriteFindObject SpriteOne = SpriteFindObject(TEXT("/Game/Sprites/FirstCubeSprite.FirstCubeSprite"));
    
    for (int i = 0; i < 10; i++) {
        int32 random = rand() % 2;
        FName ComponentName = FName(TEXT("SpriteComponent"), i);
        SpriteComponent[i] = CreateDefaultSubobject<UPaperSpriteComponent>(ComponentName);
        SpriteComponent[i]->SetSprite(random == 0 ? Sprite.Get() : SpriteOne.Get());
        SpriteComponent[i]->AttachTo(RootComponent);
        FVector2D location = FVector2D(GetActorLocation().X + i * 150.0f, GetActorLocation().Y);
        SpriteComponent[i]->RelativeLocation = FVector(location, GetActorLocation().Z);
    }
}

void AMyPaperSpriteActorController::BeginPlay() {
    Super::BeginPlay();
}

