// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperSpriteActor.h"
#include "MyPaperSpriteActorController.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API AMyPaperSpriteActorController : public APaperSpriteActor
{
	GENERATED_BODY()
	
    AMyPaperSpriteActorController();

public:
    virtual void BeginPlay() override;
    
    UPaperSpriteComponent* SpriteComponent[10];
};
