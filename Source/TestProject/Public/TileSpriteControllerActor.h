// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyPaperSpriteActor.h"
#include "PaperSpriteActor.h"
#include "TileSpriteControllerActor.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API ATileSpriteControllerActor : public APaperSpriteActor
{
	GENERATED_BODY()
	
    ATileSpriteControllerActor();
    
public:
    virtual void BeginPlay() override;
    
//    UPROPERTY(EditAnywhere)
//    int32 Height;
//    
//    UPROPERTY(EditAnywhere)
//    int32 Width;
    
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Grooper Game")
    TArray<AMyPaperSpriteActor *> TileActors;
	
    UFUNCTION(BlueprintCallable, Category = "Clickable")
    void MouseClick(int32 count);
    
};
