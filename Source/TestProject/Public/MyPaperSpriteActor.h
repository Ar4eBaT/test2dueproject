// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperSpriteActor.h"
#include "MyPaperSpriteActor.generated.h"

/**
 * 
 */
UCLASS()
class TESTPROJECT_API AMyPaperSpriteActor : public APaperSpriteActor
{
	GENERATED_BODY()
    int32 Number;
public:
    // Sets default values for this actor's properties
    AMyPaperSpriteActor();
    
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    
    // Called every frame
    virtual void Tick( float DeltaSeconds ) override;
    
    UTextRenderComponent* NumberText;
    
    UFUNCTION(BlueprintCallable, Category = "Dice")
    int32 GetNumber() const;
    
    UFUNCTION(BlueprintCallable, Category = "Dice")
    void SetText();
};
