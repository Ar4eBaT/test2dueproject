// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "MyPaperSpriteActor.h"
#include "GameFramework/GameMode.h"
#include "RandomNumberGameMode.generated.h"

/**
 *
 */
UCLASS()
class TESTPROJECT_API ARandomNumberGameMode : public AGameMode
{
	GENERATED_BODY()
    
    /** Remove the current menu widget and create a new one from the specified class, if provided. */
    void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);
    
public:
    /** Called when the game starts. */
    virtual void BeginPlay() override;
    
    // Call CurrentActor childs method to set random number text
    UFUNCTION(BlueprintCallable, Category = "UMG Game")
    void ChangeRandomNumber();
    
protected:
    /** The widget class we will use as our menu when the game starts. */
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
    TSubclassOf<UUserWidget> StartingWidgetClass;
};
